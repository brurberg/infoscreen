package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"os/exec"
	"runtime"
	"strconv"
	"strings"
	"time"

	"gopkg.in/abiosoft/ishell.v2"
)

var CONN_HOST string
var CONN_PORT string
var CONN_TYPE string
var isServer bool

func main() {
	flag.BoolVar(&isServer, "s", false, "Set to server")
	flag.StringVar(&CONN_HOST, "h", "localhost", "Set to server address")
	flag.StringVar(&CONN_PORT, "p", "3333", "Set to server port")
	flag.StringVar(&CONN_TYPE, "t", "tcp", "Set to server type (protocol)")
	flag.Parse() // after declaring flags we need to call it
	if isServer {
		server()
	} else {
		client()
	}
}

func server() {
	// Listen for incoming connections.
	l, err := net.Listen(CONN_TYPE, CONN_HOST+":"+CONN_PORT)
	if err != nil {
		fmt.Println("Error listening:", err.Error())
		os.Exit(1)
	}
	// Close the listener when the application closes.
	defer l.Close()
	fmt.Println("Listening on " + CONN_HOST + ":" + CONN_PORT)
	newConn := make(chan net.Conn, 5)
	newCommand := command{
		text: make(chan string),
		resp: make(chan respons),
	}
	go handleClient(newConn, newCommand)
	go handleUserInput(newCommand)
	for {
		// Listen for an incoming connection.
		conn, err := l.Accept()
		if err != nil {
			fmt.Println("Error accepting: ", err.Error())
			//os.Exit(1)
			continue
		}
		defer conn.Close()
		// Handle connections in a new goroutine.
		newConn <- conn
	}
}

type screen struct {
	conn net.Conn
	name string
	g    *group
}

func (s screen) String() string {
	if s.g != nil {
		return fmt.Sprintf("Screen: %s, Group: %s, Conn: %v.", s.name, s.g.name, s.conn)
	}
	return fmt.Sprintf("Screen: %s, Conn: %v.", s.name, s.conn)
}

type group struct {
	screens []*screen
	name    string
}

type respons struct {
	text string
	err  error
}

type command struct {
	text chan string
	resp chan respons
}

// Handles incoming requests.
func handleClient(newConn <-chan net.Conn, com command) {
	var screens []screen
	groups := make(map[string]group)
	for {
		select {
		case conn := <-newConn:
			screens = append(screens, screen{conn: conn, name: "", g: nil})
		case text := <-com.text:
			line := strings.Split(strings.TrimSpace(text), " ")
			switch line[0] {
			case "set":
				switch line[1] {
				case "page":
					if len(line) > 4 {
						com.resp <- respons{text: "", err: fmt.Errorf("To many arguments")}
						break
					}
					resp := ""
					if len(line) < 4 {
						if len(groups) == 0 {
							com.resp <- respons{text: "", err: fmt.Errorf("No groups")}
							break
						}
						first := true
						for _, gr := range groups {
							if first {
								resp = fmt.Sprintf("%v", gr.name)
								first = false
								continue
							}
							resp = fmt.Sprintf("%s %v", resp, gr.name)
						}
						com.resp <- respons{text: resp, err: nil}
						break
					}
					g, ok := groups[line[3]]
					if !ok {
						com.resp <- respons{text: "", err: fmt.Errorf("%s is not a valid group.", strings.Title(line[3]))}
						break
					}
					for _, s := range g.screens {
						s.conn.Write([]byte(line[2] + "\n"))
					}
					com.resp <- respons{"", nil}
				case "con":
					switch line[2] {
					case "group":
						if len(line) > 5 {
							com.resp <- respons{text: "", err: fmt.Errorf("To many arguments")}
							break
						}
						resp := ""
						if len(line) == 3 {
							if len(screens) == 0 {
								com.resp <- respons{text: "", err: fmt.Errorf("No screans")}
								break
							}
							if screens[0].name == "" {
								resp = fmt.Sprintf("%d", 0)
							} else {
								resp = fmt.Sprintf("%s", screens[0].name)
							}
							for i, scr := range screens[1:] {
								if scr.name == "" {
									resp = fmt.Sprintf("%s %d", resp, i+1)
								} else {
									resp = fmt.Sprintf("%s %s", resp, scr.name)
								}
							}
							resp += ";"
						}
						if len(line) < 5 {
							if len(groups) == 0 {
								com.resp <- respons{text: "", err: fmt.Errorf("No groups")}
								break
							}
							first := true
							for _, gr := range groups {
								if first {
									if resp == "" {
										resp = fmt.Sprintf("%v", gr.name)
									} else {
										resp = fmt.Sprintf("%s%v", resp, gr.name)
									}
									first = false
									continue
								}
								resp = fmt.Sprintf("%s %v", resp, gr.name)
							}
							com.resp <- respons{text: resp, err: nil}
							break
						}
						i, err := strconv.Atoi(line[3])
						if err != nil {
							i = -1
							for j, scr := range screens {
								if scr.name == line[3] {
									i = j
									break
								}
							}
							if i == -1 {
								com.resp <- respons{text: "", err: fmt.Errorf("Arg 4 is neither a number nor a valid screen name.")}
								break
							}
						}
						if len(screens) <= i {
							com.resp <- respons{text: "", err: fmt.Errorf("Con %d is out of screen range.", i)}
							break
						}
						g, ok := groups[line[4]]
						if !ok {
							com.resp <- respons{text: "", err: fmt.Errorf("%s is not a valid group.", strings.Title(line[4]))}
							break
						}
						g.screens = append(g.screens, &screens[i])
						groups[line[4]] = g
						com.resp <- respons{"", nil}
						//newScreens = append(newScreens[:i], newScreens[i+1:]...)
						//newError <- nil
					case "name":
						if len(line) > 5 {
							com.resp <- respons{text: "", err: fmt.Errorf("To many arguments")}
							break
						}
						if len(line) == 3 {
							if len(screens) == 0 {
								com.resp <- respons{text: "", err: fmt.Errorf("No screans")}
								break
							}
							resp := ""
							if screens[0].name == "" {
								resp = fmt.Sprintf("%d", 0)
							} else {
								resp = fmt.Sprintf("%s", screens[0].name)
							}
							for i, scr := range screens[1:] {
								if scr.name == "" {
									resp = fmt.Sprintf("%s %d", resp, i+1)
								} else {
									resp = fmt.Sprintf("%s %s", resp, scr.name)
								}
							}
							com.resp <- respons{text: resp, err: nil}
							break
						}
						i, err := strconv.Atoi(line[3])
						if err != nil {
							i = -1
							for j, scr := range screens {
								if scr.name == line[3] {
									i = j
									break
								}
							}
							if i == -1 {
								com.resp <- respons{text: "", err: fmt.Errorf("Arg 4 is neither a number nor a valid screen name.")}
								break
							}
						}
						if len(screens) <= i {
							com.resp <- respons{text: "", err: fmt.Errorf("Con %d is out of screen range.", i)}
							break
						}
						screens[i].name = line[4]
						com.resp <- respons{"", nil}
					}
				case "groupname":
					if len(line) > 4 {
						com.resp <- respons{text: "", err: fmt.Errorf("To many arguments")}
						break
					}
					if len(line) == 2 {
						if len(groups) == 0 {
							com.resp <- respons{text: "", err: fmt.Errorf("No groups")}
							break
						}
						resp := ""
						first := true
						for _, gr := range groups {
							if first {
								resp = fmt.Sprintf("%v", gr.name)
								first = false
								continue
							}
							resp = fmt.Sprintf("%s %v", resp, gr.name)
						}
						com.resp <- respons{text: resp, err: nil}
						break
					}
					g := groups[line[2]]
					g.name = line[3]
					groups[line[2]] = g
					com.resp <- respons{"", nil}
				}
			case "create":
				switch line[1] {
				case "group":
					if len(line) != 3 {
						com.resp <- respons{text: "", err: fmt.Errorf("Incorect amount of arguments.")}
						break
					}
					if _, ok := groups[line[2]]; ok {
						com.resp <- respons{text: "", err: fmt.Errorf("The group %s allready exists.", line[2])}
						break
					}
					groups[line[2]] = group{screens: nil, name: line[2]}
					com.resp <- respons{"", nil}
					//newError <- nil
				}
			case "list":
				switch line[1] {
				case "con":
					if line[2] == "new" {
						resp := ""
						for _, s := range screens {
							if s.name != "" {
								continue
							}
							if resp == "" {
								resp = fmt.Sprintf("%v", s)
								continue
							}
							resp = fmt.Sprintf("%s\n%v", resp, s)
						}
						com.resp <- respons{text: resp, err: nil}
						break
					}
					if line[2] == "all" {
						resp := ""
						for _, s := range screens {
							if resp == "" {
								resp = fmt.Sprintf("%v", s)
								continue
							}
							resp = fmt.Sprintf("%s\n%v", resp, s)
						}
						com.resp <- respons{text: resp, err: nil}
						break
					}
					g, ok := groups[line[2]]
					if !ok {
						com.resp <- respons{text: "", err: fmt.Errorf("%s is not a valid group.", strings.Title(line[2]))}
						break
					}
					resp := ""
					for _, s := range g.screens {
						if resp == "" {
							resp = fmt.Sprintf("%v", s)
							continue
						}
						resp = fmt.Sprintf("%s\n%v", resp, s)
					}
					com.resp <- respons{text: resp, err: nil}
				case "group":
					resp := ""
					for g := range groups {
						if resp == "" {
							resp = fmt.Sprintf("%s", g)
							continue
						}
						resp = fmt.Sprintf("%s\n%s", resp, g)
					}
					com.resp <- respons{text: resp, err: nil}
				}
			default:
				if line[0] == "" {
					com.resp <- respons{text: "", err: fmt.Errorf("Please provide a command.")}
				} else {
					com.resp <- respons{text: "", err: fmt.Errorf("%s is not a command.", line[0])}
				}
			}
			//newError <- nil
		}
	}
}

func handleUserInput(com command) {
	shell := ishell.New()
	shell.SetHomeHistoryPath(".ishell_history")

	shell.Println("Simple iShell for Infoscreens")

	create := &ishell.Cmd{
		Name:    "create",
		Aliases: []string{"creat", "c"},
		Help:    "Master create command",
	}
	create.AddCmd(&ishell.Cmd{
		Name:    "group",
		Aliases: []string{"g"},
		Help:    "Creates a new group",
		Func: func(c *ishell.Context) {
			if len(c.Args) != 1 {
				handleResp(c, respons{text: "", err: fmt.Errorf("Wrong amount of arguments")})
				return
			}
			com.text <- "create group " + strings.Join(c.Args, " ")
			handleResp(c, <-com.resp)
		},
	})
	shell.AddCmd(create)

	set := &ishell.Cmd{
		Name:    "set",
		Aliases: []string{"s"},
		Help:    "master set command",
	}
	set_con := &ishell.Cmd{
		Name:    "connection",
		Aliases: []string{"con", "c"},
		Help:    "master set connection command",
	}
	set_con.AddCmd(&ishell.Cmd{
		Name: "group",
		Help: "Sets the group of a connection",
		Func: func(c *ishell.Context) {
			if len(c.Args) > 2 {
				c.Println("To many arguments")
				return
			}
			com.text <- "set con group " + strings.Join(c.Args, " ")
			if len(c.Args) != 2 {
				resp := <-com.resp
				if resp.err != nil {
					handleResp(c, resp)
					return
				}
				data := strings.Split(resp.text, ";")
				if len(c.Args) == 0 {
					conections := strings.Split(data[0], " ")
					chosenCon := c.MultiChoice(conections, "Whitch connection?")
					groups := strings.Split(data[1], " ")
					chosenGr := c.MultiChoice(groups, "Whitch group?")
					com.text <- fmt.Sprintf("set con group %s %s", conections[chosenCon], groups[chosenGr])
				} else {
					groups := strings.Split(data[0], " ")
					chosenGr := c.MultiChoice(groups, "Whitch group?")
					com.text <- fmt.Sprintf("set con group %s %s", c.Args[0], groups[chosenGr])
				}
			}
			handleResp(c, <-com.resp)
		},
	})
	set_con.AddCmd(&ishell.Cmd{
		Name: "name",
		Help: "Sets a name for a connection",
		Func: func(c *ishell.Context) {
			if len(c.Args) > 2 {
				c.Println("To many arguments")
				return
			}
			com.text <- "set con name " + strings.Join(c.Args, " ")
			if len(c.Args) != 2 {
				resp := <-com.resp
				if resp.err != nil {
					handleResp(c, resp)
					return
				}
				if len(c.Args) == 0 {
					conections := strings.Split(resp.text, " ")
					chosenCon := c.MultiChoice(conections, "Whitch connection?")
					c.Print("Name: ")
					name := " "
					for strings.Contains(name, " ") {
						name = c.ReadLine()
						if strings.Contains(name, " ") {
							c.Println("Names cannot contain spaces.")
						}
					}
					com.text <- fmt.Sprintf("set con name %s %s", conections[chosenCon], name)
				} else {
					c.Printf("Ambigious amount of inputs.\n%s\n", c.Cmd.Help)
					return
				}
			}
			handleResp(c, <-com.resp)
		},
	})
	set.AddCmd(set_con)

	set_groupname := &ishell.Cmd{
		Name:    "groupname",
		Aliases: []string{"gn"},
		Help:    "Sets groups name",
		Func: func(c *ishell.Context) {
			if len(c.Args) > 2 {
				c.Println("To many arguments")
				return
			}
			com.text <- "set groupname " + strings.Join(c.Args, " ")
			if len(c.Args) != 2 {
				resp := <-com.resp
				if resp.err != nil {
					handleResp(c, resp)
					return
				}
				if len(c.Args) == 0 {
					groups := strings.Split(resp.text, " ")
					chosenGr := c.MultiChoice(groups, "Whitch group?")
					c.Print("Name: ")
					name := " "
					for strings.Contains(name, " ") {
						name = c.ReadLine()
						if strings.Contains(name, " ") {
							c.Println("Names cannot contain spaces.")
						}
					}
					com.text <- fmt.Sprintf("set groupname %s %s", groups[chosenGr], name)
				} else {
					c.Printf("Ambigious amount of inputs.\n%s\n", c.Cmd.Help)
					return
				}
			}
			handleResp(c, <-com.resp)
		},
	}
	set.AddCmd(set_groupname)

	set_page := &ishell.Cmd{
		Name:    "page",
		Aliases: []string{"p"},
		Help:    "Sets a new page for a group",
		Func: func(c *ishell.Context) {
			if len(c.Args) > 2 {
				c.Println("To many arguments")
				return
			}
			com.text <- "set page " + strings.Join(c.Args, " ")
			if len(c.Args) != 2 {
				resp := <-com.resp
				if resp.err != nil {
					handleResp(c, resp)
					return
				}
				groups := strings.Split(resp.text, " ")
				chosenGr := c.MultiChoice(groups, "Whitch group?")
				if len(c.Args) == 0 {
					c.Print("Name: ")
					url := " "
					for strings.Contains(url, " ") {
						url = c.ReadLine()
						if strings.Contains(url, " ") {
							c.Println("URLs cannot contain spaces.")
						}
					}
					com.text <- fmt.Sprintf("set page %s %s", url, groups[chosenGr])
				} else {
					com.text <- fmt.Sprintf("set page %s %s", c.Args[0], groups[chosenGr])
				}
			}
			handleResp(c, <-com.resp)
		},
	}
	set.AddCmd(set_page)
	shell.AddCmd(set)

	list := &ishell.Cmd{
		Name: "list",
		Help: "master list command",
	}
	list.AddCmd(&ishell.Cmd{
		Name:    "connection",
		Aliases: []string{"con"},
		Help:    "master list command",
		Func: func(c *ishell.Context) {
			if len(c.Args) > 1 {
				c.Println("To many arguments")
				return
			}
			com.text <- "list con " + strings.Join(c.Args, " ")
			handleResp(c, <-com.resp)
		},
	})
	list.AddCmd(&ishell.Cmd{
		Name: "groups",
		Help: "Prints all groups",
		Func: func(c *ishell.Context) {
			if len(c.Args) > 0 {
				c.Println("To many arguments")
				return
			}
			com.text <- "list groups "
			handleResp(c, <-com.resp)
		},
	})
	shell.AddCmd(list)

	shell.Run()
	os.Exit(0)
}

func handleResp(c *ishell.Context, resp respons) {
	if resp.err != nil {
		c.Printf("Err: %v\n", resp.err)
		if resp.text == "" {
			c.Println(c.Cmd.Help)
		}
	}
	if resp.text != "" {
		c.Println(resp.text)
	}
}

func client() {
	for {
		conn, err := net.Dial(CONN_TYPE, CONN_HOST+":"+CONN_PORT)
		if err != nil {
			fmt.Println(err)
			time.Sleep(1 * time.Second)
			continue
		}

		connbuf := bufio.NewReader(conn)
		var cmd *exec.Cmd
		for {
			str, err := connbuf.ReadString('\n')
			if len(str) > 0 {
				if cmd != nil {
					err = cmd.Process.Kill()
					if err != nil {
						log.Printf("Command finished with error: %v", err)
					}
				}
				fmt.Println(str)
				if runtime.GOOS == "windows" {
					cmd = exec.Command("cmd.exe", "/C", "start", "firefox.exe", "-silent", "-private-window", "--kiosk", str)
				} else {
					cmd = exec.Command("firefox", "-silent", "-private-window", "--kiosk", str)
				}
				err := cmd.Start()
				if err != nil {
					log.Fatal(err)
				}
			}
			if err != nil {
				log.Println(err, cmd)
				if cmd != nil {
					log.Println("Killing process")
					err = cmd.Process.Kill()
					if err != nil {
						log.Printf("Command finished with error: %v", err)
					}
				}
				break
			}
		}
	}
}
